import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  
  title = 'my-app';

  formulario: any;

  constructor(private fb: FormBuilder, private http: HttpClient) {}


  ngOnInit(): void {
    this.formulario = this.fb.group({
      document: '',
      docType: '',
      email: [''/*, [Validators.pattern('[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}')]*/],
      phone: [''/*, [Validators.pattern('[3]+[0-9]')]*/] 
    });
  }

  registerData() {
      this.http.post('http://localhost:9898/api/customer', this.formulario.value)
      .subscribe(data => {
        console.log(data);
      })
  }


}
